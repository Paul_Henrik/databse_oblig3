<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>
<!-- Drop Down -->
<body>
<style>
.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    padding: 12px 16px;
    z-index: 1;
}

.dropdown:hover .dropdown-content {
    display: block;
}
</style>

<div class="dropdown">
  <span>Mouse over me</span>
  <div class="dropdown-content">
    <p><?php echo $_POST['cars'];?></p>
  </div>
</div>
<br><br><br><br><br><br>
<i>Next Example: select form</i>
<!-- Drop Down End-->

<!-- SelectBox -->
<form action="/testSide.php" method="post">
  <select name="cars">
    <option value="volvo">Volvo</option>
    <option value="saab">Saab</option>
    <option value="fiat">Fiat</option>
    <option value="audi">Audi</option>
  </select>
  <br><br>
  <input type="submit">
</form>
<?php
	if(isset($_POST['cars']))
	   echo $_POST['cars'];
	?>
	
</body>
</html>